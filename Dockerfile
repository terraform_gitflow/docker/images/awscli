FROM alpine

RUN apk add --no-cache --update \
    py-pip && \
    pip install --upgrade awscli